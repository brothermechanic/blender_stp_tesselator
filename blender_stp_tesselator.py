# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 brothermechanic@gmail.com

Created by brothermechanic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
"""
DESCRIPTION
This script works only with freecad and bledner on same Python version!
The script search converts stpfile solid to temporary stlfile,
then tesselate stl mesh, and save it to same dit in objfile.
"""

import os
import tempfile
import argparse
import FreeCAD
import Part
import Mesh
import bpy


class ArgumentParserForBlender(argparse.ArgumentParser):
    """
    This class is identical to its superclass, except for the parse_args
    method (see docstring). It resolves the ambiguity generated when calling
    Blender from the CLI with a python script, and both Blender and the script
    have arguments. E.g., the following call will make Blender crash because
    it will try to process the script's -a and -b flags:
    >>> blender --python my_script.py -a 1 -b 2

    To bypass this issue this class uses the fact that Blender will ignore all
    arguments given after a double-dash ('--'). The approach is that all
    arguments before '--' go to Blender, arguments after go to the script.
    The following calls work fine:
    >>> blender --python my_script.py -- -a 1 -b 2
    >>> blender --python my_script.py --
    """

    @staticmethod
    def _get_argv_after_doubledash():
        """
        Given the sys.argv as a list of strings, this method returns the
        sublist right after the '--' element (if present, otherwise returns
        an empty list).
        """
        try:
            idx = sys.argv.index("--")
            return sys.argv[idx+1:]  # the list after '--'
        except ValueError as e:  # '--' not in the list:
            return None

    # overrides superclass
    def parse_args(self, args=None, namespace=None):
        """
        This method is expected to behave identically as in the superclass,
        except that the sys.argv list will be pre-processed using
        _get_argv_after_doubledash before. See the docstring of the class for
        usage examples and details.
        """
        return super().parse_args(
            args=args or self._get_argv_after_doubledash(),
            namespace=namespace
        )


def cleanup():
    for obj in bpy.data.objects:
        bpy.data.objects.remove(obj, do_unlink=True)


def stp2mesh(stpfile, stlfile):
    shape = Part.Shape()
    shape.read(stpfile)
    doc = FreeCAD.newDocument('Doc')
    pf = doc.addObject("Part::Feature", "MyShape")
    pf.Shape = shape
    Mesh.export([pf], stlfile)


def stp2obj(stlfile, voxel_size , path, basename):
    bpy.ops.import_mesh.stl(filepath=stlfile, global_scale=0.001)
    bpy.ops.object.modifier_add(type='REMESH')
    bpy.context.object.modifiers["Remesh"].voxel_size = voxel_size
    objfile = os.path.join(path, f'{basename}.obj')
    bpy.ops.wm.obj_export(filepath=objfile,
                          forward_axis='Y', up_axis='Z', scaling_factor=1000)


def stp2mesh2obj(stpfile, voxel_size):
    path, filename = os.path.split(args.stpfile)
    basename = filename.split(".")[0]
    stlfile = os.path.join(tempfile.gettempdir(), f'{basename}.stl')
    cleanup()
    stp2mesh(stpfile, stlfile)
    stp2obj(stlfile, voxel_size, path, basename)


if __name__ == '__main__':
    parser = ArgumentParserForBlender()
    parser.add_argument('-arg_stp', '--stpfile', required=True)
    parser.add_argument('-arg_voxel', '--voxel_size', required=True)
    args = parser.parse_args()

    stp2mesh2obj(args.stpfile, float(args.voxel_size))

    print("__________Tesselation Done!__________")
