#!/usr/bin/python3.10
# -*- coding: utf-8 -*-
"""
Copyright (C) 2022 brothermechanic@gmail.com

Created by brothermechanic

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
"""
DESCRIPTION
Run blender as regular lib in background with scripts and options
"""
import subprocess

# path to bledner binary
blender_exe = '/usr/bin/blender'
# script path
script = 'blender_stp_tesselator.py'
# path to stp file
stpfile = '/media/disk/robossembler/stp2obj/hat.step'
# tesselation block size in metters
voxel_size = '0.001'


blender_args = [
blender_exe, "-b",
"--python", script, "--",
"--stpfile", stpfile,
"--voxel_size", voxel_size,
]
subprocess.call(blender_args)
